﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Lab1
{
    class Trapeze
    {
        public static void Draw_Trapeze(Bitmap image, PointF[] points, PointF[] H, float height, float smaller_base, ColorDialog colorDialog)
        {
            double A = 0, B = 0, C = 0;
            H = new PointF[] { new PointF(), new PointF() };
            PointF medium_12 = new PointF((points[0].X + points[1].X) / 2, (points[0].Y + points[1].Y) / 2);
            PointF medium_34 = new PointF();
            Pen pen = new Pen(Color.Black, 2);

            Search_ABC(ref A, ref B, ref C, points[0], points[1]);
            Search_Point(ref medium_34, A, B, C, points[0], points[1], medium_12, height);
            medium_34 = Symmetry(medium_34, medium_12);
            Search_ABC(ref A, ref B, ref C, medium_12, medium_34);
            Search_Point(ref points[2], A, B, C, medium_12, medium_34, medium_34, smaller_base / 2);
            points[3] = Symmetry(points[2], medium_34);
            //малюєм трапецію
            PointF[] trapeze = new PointF[] { points[0], points[1], points[3], points[2] };
            Graphics.FromImage(image).DrawPolygon(pen, points);
            Graphics.FromImage(image).FillPolygon(new SolidBrush(Color.White), points);

            //малюєм висоти
            Draw_Heights(image, H, points, height);
        }


        public static void Draw_Heights(Bitmap image, PointF[] H, PointF[] points, float height)
        {
            PointF medium_12 = new PointF((points[0].X + points[1].X) / 2, (points[0].Y + points[1].Y) / 2);
            double A = 0, B = 0, C = 0;
            Pen pen = new Pen(Color.Black, 2);

            Search_ABC(ref A, ref B, ref C, points[3], points[2]);
            Search_Point(ref H[0], A, B, C, points[3], points[2], points[2], height);
            H[1] = Symmetry(H[0], medium_12);

            Graphics.FromImage(image).DrawLine(pen, points[2], H[0]);
            Graphics.FromImage(image).DrawLine(pen, points[3], H[1]);
        }
        public static PointF Symmetry(PointF point_1, PointF sym)
        {
            return new PointF(2 * sym.X - point_1.X, 2 * sym.Y - point_1.Y);
        }

        public static void Search_ABC(ref double A, ref double B, ref double C, PointF point_1, PointF point_2)
        {
            A = point_1.Y - point_2.Y;
            B = point_2.X - point_1.X;
            C = point_1.X * (point_2.Y - point_1.Y) - point_1.Y * (point_2.X - point_1.X);
        }

        public static void Search_Point(ref PointF P, double A, double B, double C, PointF point_1, PointF point_2, PointF proection_point, float height)
        {
            if (point_1.X == point_2.X)
            {
                P.X = proection_point.X + height;
                P.Y = proection_point.Y;
            }
            else if (point_1.Y == point_2.Y)
            {
                P.X = proection_point.X;
                P.Y = proection_point.Y + height;
            }
            else
            {
                P.X = Convert.ToSingle(((((height * Math.Pow(Math.Pow(A, 2) + Math.Pow(B, 2), 0.5) - C) / B) +
                proection_point.X * ((point_1.X - point_2.X) / (point_2.Y - point_1.Y)) - proection_point.Y)));
                P.X /= Convert.ToSingle(((point_1.X - point_2.X) / (point_2.Y - point_1.Y)) + (A / B));

                P.Y = Convert.ToSingle((((point_1.X - point_2.X) / (point_2.Y - point_1.Y)) * P.X - proection_point.X * ((point_1.X - point_2.X) / (point_2.Y - point_1.Y)) + proection_point.Y));
            }
        }
    }
}
