﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Lab1
{
    public partial class Form1 : Form
    {
        Bitmap image;
        public PointF[] points = null;
        public PointF[] H = new PointF[2];
        public Form1()
        {
            InitializeComponent();
            pictureBox1.Size = new Size(500, 500);
            image = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            Graphics.FromImage(image).Clear(Color.White);
            pictureBox1.Image = image;
            DrawArea.FillArea(image, pictureBox1);
            txt_A_X.Text = "50";
            txt_A_Y.Text = "100";
            txt_B_X.Text = "150";
            txt_B_Y.Text = "0";
            txt_base_length.Text = "70";
            txt_height.Text = "50";
        }

        private void btn_draw_Click(object sender, EventArgs e)
        {
            //Graphics.FromImage(image).Clear(Color.White);
            pictureBox1.Image = image;
            //DrawArea.FillArea(image, pictureBox1);

            points = new PointF[] { new PointF(Convert.ToSingle(txt_A_X.Text) + Convert.ToSingle(pictureBox1.Width/2), -Convert.ToSingle(txt_A_Y.Text) + Convert.ToSingle(pictureBox1.Height/2)),
                                    new PointF(Convert.ToSingle(txt_B_X.Text) + Convert.ToSingle(pictureBox1.Width/2), -Convert.ToSingle(txt_B_Y.Text) + Convert.ToSingle(pictureBox1.Height/2)),
                                    new PointF(), new PointF()};

            Trapeze.Draw_Trapeze(image, points, H, Convert.ToSingle(txt_height.Text), Convert.ToSingle(txt_base_length.Text), colorDialog1);
        }

        private void btn_change_color_Click(object sender, EventArgs e)
        {
            ColorDialog color = new ColorDialog();
            if (color.ShowDialog() == DialogResult.OK)
            {
                pictureBox1.Image = image;
                if (points != null)
                {
                    Graphics.FromImage(image).FillPolygon(new SolidBrush(color.Color), points);
                    Trapeze.Draw_Heights(image, H, points, Convert.ToSingle(txt_height.Text));
                }
                else
                    MessageBox.Show("Ви не побудували трапецію!");
            }
        }
    }
}
