﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Lab1
{
    class DrawArea
    {
        public static void FillArea( Bitmap image, PictureBox pictureBox)
        {
            Pen lightgray_pen = new Pen(Color.LightGray, 1);
            Pen gray_pen = new Pen(Color.Gray, 1);
            Pen black_pen = new Pen(Color.Black, 2);

            Draw_Net(image, pictureBox, gray_pen, lightgray_pen);
            Draw_Axes(image, pictureBox, gray_pen, black_pen);
            Draw_Numeration(image, pictureBox);
        }

        public static void Draw_Axes( Bitmap image, PictureBox pictureBox, Pen gray_pen, Pen black_pen)
        {
            Graphics.FromImage(image).DrawLine(black_pen, pictureBox.Width / 2, 0, pictureBox.Width / 2, pictureBox.Height);
            Graphics.FromImage(image).DrawLine(black_pen, 0, pictureBox.Height / 2, pictureBox.Width, pictureBox.Height / 2);

            for (int i = 0; i < pictureBox.Width; i += 10)
            {
                Graphics.FromImage(image).DrawLine(gray_pen, i, pictureBox.Height / 2 - 5, i, pictureBox.Height / 2 + 5);
                if (i % 50 == 0)
                    Graphics.FromImage(image).DrawLine(black_pen, i, pictureBox.Height / 2 - 5, i, pictureBox.Height / 2 + 5);
            }

            for (int i = 0; i < pictureBox.Height; i += 10)
            {
                Graphics.FromImage(image).DrawLine(gray_pen, pictureBox.Width / 2 - 5, i, pictureBox.Width / 2 + 5, i);
                if (i % 50 == 0)
                    Graphics.FromImage(image).DrawLine(black_pen, pictureBox.Width / 2 - 5, i, pictureBox.Width / 2 + 5, i);
            }

            PointF[] x_arrow = { new PointF(pictureBox.Width ,pictureBox.Height/2),
                                new PointF(pictureBox.Width - 10, pictureBox.Height/2 - 5),
                                new PointF(pictureBox.Width - 10, pictureBox.Height/2 + 5) };

            PointF[] y_arrow = { new PointF(pictureBox.Width/2, 0),
                                new PointF(pictureBox.Width/2 - 5, 10),
                                new PointF(pictureBox.Width/2 + 5, 10) };

            Graphics.FromImage(image).FillPolygon(Brushes.Black, x_arrow);
            Graphics.FromImage(image).DrawString("X", new Font("TimesNewRoman", 10), Brushes.Black, pictureBox.Width -15, pictureBox.Height / 2 +5);
            Graphics.FromImage(image).FillPolygon(Brushes.Black, y_arrow);
            Graphics.FromImage(image).DrawString("Y", new Font("TimesNewRoman", 10), Brushes.Black, pictureBox.Width / 2 +5, 0);
        }

        public static void Draw_Net(Bitmap image, PictureBox pictureBox, Pen gray_pen, Pen lightgray_pen)
        {
            for (int i = 0; i < pictureBox.Height; i += 10)
            {
                if (i % 50 == 0)
                    Graphics.FromImage(image).DrawLine(gray_pen, 0, i, pictureBox.Width, i);
                else
                    Graphics.FromImage(image).DrawLine(lightgray_pen, 0, i, pictureBox.Width, i);
            }

            for (int i = 0; i < pictureBox.Width; i += 10)
            {
                if (i % 50 == 0)
                    Graphics.FromImage(image).DrawLine(gray_pen, i, 0, i, pictureBox.Height);
                else
                    Graphics.FromImage(image).DrawLine(lightgray_pen, i, 0, i, pictureBox.Height);
            }
        }

        public static void Draw_Numeration( Bitmap image, PictureBox pictureBox)
        {
            Graphics.FromImage(image).DrawString("0", new Font("TimesNewRoman", 10), new SolidBrush(Color.Black), pictureBox.Width / 2, pictureBox.Height / 2);

            for (int i = 50; i < pictureBox.Height / 2; i += 50)
            {
                Graphics.FromImage(image).DrawString(Convert.ToString(i), new Font("TimesNewRoman", 10), Brushes.Black, pictureBox.Width / 2, pictureBox.Height / 2 - i);
                Graphics.FromImage(image).DrawString(Convert.ToString("-" + i), new Font("TimesNewRoman", 10), Brushes.Black, pictureBox.Width / 2, pictureBox.Height / 2 + i);
            }

            for (int i = 50; i < pictureBox.Width / 2; i += 50)
            {
                Graphics.FromImage(image).DrawString("-" + Convert.ToString(i), new Font("TimesNewRoman", 10), Brushes.Black, pictureBox.Width / 2 - i, pictureBox.Height / 2);
                Graphics.FromImage(image).DrawString(Convert.ToString(i), new Font("TimesNewRoman", 10), Brushes.Black, pictureBox.Width / 2 + i, pictureBox.Height / 2);
            }
        }
    }
}
